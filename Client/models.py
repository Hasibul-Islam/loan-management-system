from django.db import models


class Client(models.Model):
	firstName = models.CharField(max_length=100)
	lastName = models.CharField(max_length=100)
	email = models.EmailField(unique=True)
	actype = models.CharField(max_length=100)
	loan = models.DecimalField(max_digits=10, decimal_places=0)
	duration = models.DecimalField(max_digits=10, decimal_places=0)
	phone = models.CharField(max_length=11)
	per_month_to_pay = models.DecimalField(max_digits=10,decimal_places=0)

	def __str__(self):
		return self.email

class Contact(models.Model):
	firstName = models.CharField(max_length=100)
	lastName = models.CharField(max_length=100)
	email = models.EmailField()
	phone = models.CharField(max_length=11)
	msg = models.CharField(max_length=1000)

	def __str__(self):
		return self.firstName

class Record(models.Model):

	email = models.ForeignKey(Client, on_delete=models.CASCADE)
	submitted_money = models.DecimalField(max_digits=10,decimal_places=0)