from django.shortcuts import render,redirect
from django.http import HttpResponse
from .models import Client,Contact,Record
# Create your views here.


def clientsubmit(request):


	firstName = request.POST["firstName"]
	lastName = request.POST["lastName"]
	email = request.POST["email"]
	actype = request.POST["actype"]
	loan = request.POST["loan"]
	loan = int(loan)
	loan = (loan*5)/100+loan
	loan = int(loan)
	loan = str(loan)
	duration = request.POST["duration"]
	phone = request.POST["phone"]
	per_month_to_pay = int(loan)/12
	per_month_to_pay = str(per_month_to_pay)


	clientinfo = Client(firstName=firstName,lastName=lastName,email=email,actype=actype,loan=loan,duration=duration,phone=phone,per_month_to_pay=per_month_to_pay)
	clientinfo.save()

	

	return render(request,'client.html')

def contactsubmit(request):
	firstName = request.POST["firstName"]
	lastName = request.POST["lastName"]
	email = request.POST["email"]
	phone = request.POST["phone"]
	msg = request.POST["msg"]

	contactinfo = Contact(firstName=firstName,lastName=lastName,email=email,phone=phone,msg=msg)
	contactinfo.save()

	return render(request,'contact.html')


def service(request):

	clients = Client.objects.all().order_by('email')

	q = request.GET.get("q")
	if q:
		clients = Client.objects.filter(email=q)
	else:
		q = ""

	context = {
		'clients':clients,
		'q':q,

		}

	return render(request,'service.html',context)

def record(request, id):

	# email = request.POST["email"]
	# submitted_money = request.POST["submitted_money"]

	# recordinfo = Record(email=email,submitted_money=submitted_money)
	# recordinfo.save()

	clientobject = Client.objects.get(id=id)

	if request.method == 'POST':
		submitted_money = request.POST["submitted_money"]
		recordinfo = Record.objects.create(email=clientobject,submitted_money=submitted_money)
		clientobject.loan = int(clientobject.loan) - int(submitted_money)
		# clientobject.loan = str(clientobject.loan)
		clientobject.save()

		return redirect("/service/")

	context = {
		'clientobject':clientobject

	}

	return render(request,'record.html',context)






