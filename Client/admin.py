from django.contrib import admin
from .models import Client,Contact

admin.site.register(Client)
admin.site.register(Contact)

# Register your models here.
